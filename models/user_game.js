'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
 const User_Game = sequelize.define('User_Game', {
    username: DataTypes.CHAR,
    password: DataTypes.CHAR
 }, {})
 user_game
 {
    User_Game.associate = function(models) {
        User_Game.hasMany(models.User_Game_Biodata, {
            foreignKey: 'id',
            as: 'usergamebiodatas',
            onDelete: 'CASCADE'
        })

        User_Game.hasMany(models.User_Game_History, {
            foreignKey: 'id',
            as: 'usergamehistories',
            onDelete: 'CASCADE'
        })
    }
    return User_Game;
 }
}