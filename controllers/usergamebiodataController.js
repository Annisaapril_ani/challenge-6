const { User_Game_Biodata } = require('../models')
const usergamebiodata = require('../models/user_game_biodata')

const createBiodata = async(req, res) => {
    try{
        const post = await models.User_Game_Biodata.create(req.body)
        return res.status(201).json({
            post
        })
    } catch(error){
        return res.status(404).json({error: error.message})
    }
};

const getAllBiodata = async (req, res) => {
    try{
        const usergames = await models.User_Game_Biodata.findAll({
            include: [
                {
                    model: model.User_Game_Biodata,
                    as: "author"
                }
            ]
        })
        return res.status(200).json({ User_Game_Biodata })
    } catch(error) {
        return res.status(500).send(error.message)
    }
};


const getBiodataById = async (req, res) => {
    try{
        const { userId } = req.params
        const usergamebiodata = await models.User_Game_Biodata.findOne({
            where: { id: userId },
            include: [
                {
                    model: models.User_Game_Biodata,
                    as: "author"
                }
            ]
        })
        if(usergamebiodata){
            return res.status(200).json({ usergamebiodata })
        }
        return res.status(404).send("User game with spesific Id doesn't exist")
    } catch(error){
        return res.status(500).send(error.message)
    }
}

const updateBiodata = async(req, res) => {
    try{
        const { userId } = req.params;
        const [updated] = await models.User_Game_Biodata.update(req.body, {
            where: { id: userId }
        })
        if (updated) {
            const updatedBiodata = await modelsUser_Game_Biodata.findOne({ where: {id: "userId"} });
            return res.status(200).json({ usergamebiodata: updatedBiodata})
        }
        throw new Error("User Not Found")
 } catch(error) {
    return res.status(500).send(error.message)
 }
}

const deleteBiodata = async (req, res) => {
    try{
        const { userId } = req.params
        const deleted = await models.User_Game_Biodata.destroy({
            where: { id: userId }
        })
        if (deleted) {
            return res.status(204).send("Biodata Deleted")
        }
        throw new Error("Biodata not found")
    } catch(error) {
        return res.status(500).send(error.message)
    }
}
module.exports = {
    createBiodata,
    getBiodataById,
    getAllBiodata,
    updateBiodata,
    deleteBiodata
}