const { User_Game_History } = require('../models')
const usergamehistory = require('../models/user_game_history')

const createHistory = async(req, res) => {
    try{
        const post = await models.User_Game_History.create(req.body)
        return res.status(201).json({
            post
        })
    } catch(error){
        return res.status(404).json({error: error.message})
    }
};

const getAllHistory = async (req, res) => {
    try{
        const usergames = await models.User_Game_History.findAll({
            include: [
                {
                    model: model.User_Game_History,
                    as: "author"
                }
            ]
        })
        return res.status(200).json({ User_Game_History })
    } catch(error) {
        return res.status(500).send(error.message)
    }
};


const getHistoryById = async (req, res) => {
    try{
        const { userId } = req.params
        const usergamebiodata = await models.User_Game_History.findOne({
            where: { id: userId },
            include: [
                {
                    model: models.User_Game_History,
                    as: "author"
                }
            ]
        })
        if(usergamebiodata){
            return res.status(200).json({ usergamehistory })
        }
        return res.status(404).send("User game with spesific Id doesn't exist")
    } catch(error){
        return res.status(500).send(error.message)
    }
}

const updateHistory = async(req, res) => {
    try{
        const { userId } = req.params;
        const [updated] = await models.User_Game_Biodata.update(req.body, {
            where: { id: userId }
        })
        if (updated) {
            const updatedBiodata = await modelsUser_Game_Biodata.findOne({ where: {id: "userId"} });
            return res.status(200).json({ usergamebiodata: updatedBiodata})
        }
        throw new Error("User Not Found")
 } catch(error) {
    return res.status(500).send(error.message)
 }
}

const deleteHistory = async (req, res) => {
    try{
        const { userId } = req.params
        const deleted = await models.User_Game_Biodata.destroy({
            where: { id: userId }
        })
        if (deleted) {
            return res.status(204).send("Biodata Deleted")
        }
        throw new Error("Biodata not found")
    } catch(error) {
        return res.status(500).send(error.message)
    }
}
module.exports = {
    createHistory,
    getHistoryById,
    getAllHistory,
    updateHistory,
    deleteHistory
}