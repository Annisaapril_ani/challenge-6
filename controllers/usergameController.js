const { User_Game } = require('../models')
const usergame = require('../models/user_game')

const createUserGame = async(req, res) => {
    try{
        const post = await models.User_Game.create(req.body)
        return res.status(201).json({
            post
        })
    } catch(error){
        return res.status(404).json({error: error.message})
    }
};

const getAllUserGames = async (req, res) => {
    try{
        const usergames = await models.User_Game.findAll({
            include: [
                {
                    model: model.User_Game,
                    as: "author"
                }
            ]
        })
        return res.status(200).json({ User_Game })
    } catch(error) {
        return res.status(500).send(error.message)
    }
};


const getUserGameById = async (req, res) => {
    try{
        const { userId } = req.params
        const usergame = await models.User_Game.findOne({
            where: { id: userId },
            include: [
                {
                    model: models.User_Game,
                    as: "author"
                }
            ]
        })
        if(usergame){
            return res.status(200).json({ usergame })
        }
        return res.status(404).send("User game with spesific Id doesn't exist")
    } catch(error){
        return res.status(500).send(error.message)
    }
}

const updateUserGame = async(req, res) => {
    try{
        const { userId } = req.params;
        const [updated] = await models.User_Game.update(req.body, {
            where: { id: userId }
        })
        if (updated) {
            const updatedUserGame = await models.User_Game.findOne({ where: {id: "userId"} });
            return res.status(200).json({ usergames: updatedUserGame})
        }
        throw new Error("User Not Found")
 } catch(error) {
    return res.status(500).send(error.message)
 }
}

const deleteUserGame = async (req, res) => {
    try{
        const { userId } = req.params
        const deleted = await models.User_Game.destroy({
            where: { id: userId }
        })
        if (deleted) {
            return res.status(204).send("Post Deleted")
        }
        throw new Error("Post not found")
    } catch(error) {
        return res.status(500).send(error.message)
    }
}
module.exports = {
    createUserGame,
    getUserGameById,
    getAllUserGames,
    updateUserGame,
    deleteUserGame
}