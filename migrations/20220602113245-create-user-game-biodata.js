'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('User_Game_Biodata', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      email: {
        type: Sequelize.CHAR
      },
      nama_user: {
        type: Sequelize.CHAR
      },
      tanggal_lahir: {
        allowNull: true,
        type: Sequelize.DATE
      },
      no_hp: {
        type: Sequelize.CHAR
      },
      createdAt: {
        allowNull: true,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: true,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('User_Game_Biodata');
  }
};