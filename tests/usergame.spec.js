const request = require('supertest')
const app = require('../server')

//Testing 1 
describe('Endpoints Post', () => {
    it('Should create a new user', async () => {
        const res = await request(app)
        .post('/v1/api/user_game')
        .send({
            username: 'annisaapril_ani',
            password: 'Annisa123'
        })
        expect(res.statusCode).toEqual(201)
        expect(res.body).toHaveProperty('user_game')
    })
    //Testing 2 (Find All User)
    it('should fetch all user', async () => {
        const res = await request(app).get('/v1/api/user_game')
        expect(res.statusCode).toEqual(200)
        expect(res.body).toHaveProperty('user_game')
        expect(res.body.user_game).toHaveLength(1)
    })

    //Testing 3 (Find User By Id)
    it('should fetch a single user', async () => {
        const id = 1
        const res = await request(app).get('/v1/api/:id')
        expect(res.statusCode).toEqual(200)
        expect(res.body).toHaveProperty('user_game')
    })

    //Testing 4 (Update User)
    it('should update user game', async () => {
        const res = await request(app)
        .put('/v1/api/posts/1')
        .send({
            id: 1,
            username: 'annisaapril_ani',
            password: 'Annisa123',
        })
        expect(res.statusCode).toEqual(200);
        expect(res.body).toHaveProperty('user_game');
        expect(res.body.user_game).toHaveProperty('username', 'updated username')
    })

    //Testing 5 (Delete User)
    it('should delete a user game', async () => {
        const res = await request(app).delete('/v1/api/user_game/1');
        expect(res.statusCode).toEqual(204)
    })
    it('should respond with status code 404 if resource is not found', async () => {
        const Id = 1
        const res = await request(app).get('/v1/api/user_game/:id')
        expect(res.statusCode).toEqual(404)
    })

})