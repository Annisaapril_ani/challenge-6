const { Router } = require('express')
const user_game = require("../controllers/usergameController");
const user_game_biodata = require("../controllers/usergamebiodataController");
const user_game_history = require("../controllers/usergamehistoryController");

const router = Router()

router.get('/', (req, res) => res.send('Welcome'))

router.post('/usergame', user_game.createUserGame)
router.get('/usergame', user_game.getAllUserGames)
router.get('/usergame/:id', user_game.getUserGameById)
router.put('/usergame/:id', user_game.updateUserGame)
router.delete('/usergame/:id', user_game.deleteUserGame)

router.post('/usergamebiodata', user_game_biodata.createBiodata)
router.get('/usergamebiodata', user_game_biodata.getAllBiodata)
router.get('/usergamebiodata/:id', user_game_biodata.getBiodataById)
router.put('/usergamebiodata/:id', user_game_biodata.updateBiodata)
router.delete('/usergamebiodata/:id', user_game_biodata.deleteBiodata)

router.post('/usergamehistory', user_game_history.createHistory)
router.get('/usergamehistory', user_game_history.getAllHistory)
router.get('/usergamehistory/:id', user_game_history.getHistoryById)
router.put('/usergamehistory/:id', user_game_history.updateHistory)
router.delete('/usergamehistory/:id', user_game_history.deleteHistory)

module.exports = router;
